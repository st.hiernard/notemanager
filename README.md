# NoteManager

A little application made in Kotlin to test the MVVM design pattern.

Creator : Stéphane Hiernard

## Programming Langage

Kotlin

## Installation

Clone the project and open it with Android Studio.

## Architecture

- MVVM (Model View ViewModel) Architecture
- Based on Android Architecture Component approach

### Depdencies from JetPack libraries
- [Lifecycle](https://developer.android.com/jetpack/androidx/releases/lifecycle)
- [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
- [Navigation Component](https://developer.android.com/guide/navigation/navigation-migrate)
- [SafeArgs](https://developer.android.com/guide/navigation/navigation-pass-data#Safe-args)
- [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)

