package com.example.notemanager.repesitory

import androidx.lifecycle.LiveData

interface Repository<T, K> {
    fun getAll(): LiveData<List<T>>
    fun getById(id: K): LiveData<T>
    suspend fun add(entity: T)
    suspend fun update(entity: T)
    suspend fun remove(id: K)
    suspend fun removeAll()
}