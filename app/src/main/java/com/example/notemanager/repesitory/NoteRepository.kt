package com.example.notemanager.repesitory

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.example.notemanager.data.AppDatabase
import com.example.notemanager.data.Note
import java.util.*

class NoteRepository(private val appContext:Context) : Repository<Note,UUID>{

    private val noteDao = AppDatabase.getInstance(appContext).noteDao()

    override fun getAll(): LiveData<List<Note>> = noteDao.getAll()
    override fun getById(id: UUID): LiveData<Note> = noteDao.findById(id)
    override suspend fun add(note: Note) = noteDao.insert(note)
    override suspend fun update(note: Note) = noteDao.update(note)
    override suspend fun remove(id: UUID) = noteDao.delete(id)
    override suspend fun removeAll() = noteDao.deleteAll()
}


