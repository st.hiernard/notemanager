package com.example.notemanager.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.example.notemanager.data.AppDatabase
import com.example.notemanager.data.Note
import com.example.notemanager.repesitory.NoteRepository
import kotlinx.coroutines.launch
import java.text.DateFormat
import java.time.LocalDateTime
import java.util.*

class NoteListViewModel(application: Application) : AndroidViewModel(application) {

    private val noteRepository = NoteRepository(application)

    private val noteList: LiveData<List<Note>> = noteRepository.getAll()

    fun deleteAllNotes() {
        viewModelScope.launch { noteRepository.removeAll() }
    }
    fun addNote(title:String): UUID {
        val noteID = UUID.randomUUID()
        viewModelScope.launch {
            noteRepository.add(
                Note(
                    uid = noteID,
                    title = title,
                    creationDate = Calendar.getInstance().time,
                    text = ""
                )
            )
        }
        return noteID
    }
    fun getAllNotes() = noteList
}

