package com.example.notemanager.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.example.notemanager.data.Note
import com.example.notemanager.repesitory.NoteRepository
import kotlinx.coroutines.launch
import java.util.*

class NoteDetailViewModel(application: Application) : AndroidViewModel(application) {

    private val noteRepository = NoteRepository(application)

    private lateinit var currentNote: LiveData<Note>

    var isTitleSpecified:Boolean = false
    var isTextSpecified:Boolean = false
    var isCreationDateSpecified:Boolean = false

    fun getNote(id:UUID):LiveData<Note> { currentNote = noteRepository.getById(id) ; return currentNote}
    fun deleteNote(id: UUID) { viewModelScope.launch { noteRepository.remove(id) } }
    fun updateNote(text:String) { viewModelScope.launch {
        val note = currentNote.value
        if(note!=null) note.text = text
        if(note!=null)noteRepository.update(note)
    } }

}