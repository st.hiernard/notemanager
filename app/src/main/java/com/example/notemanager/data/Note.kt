package com.example.notemanager.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.text.DateFormat
import java.util.*

@Entity
data class Note (
    @PrimaryKey(autoGenerate = false) val uid: UUID,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "text") var text: String?,
    @ColumnInfo(name = "creation_date") val creationDate: Date
){
    override fun toString() = title
}