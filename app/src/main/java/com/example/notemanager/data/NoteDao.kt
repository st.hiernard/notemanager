package com.example.notemanager.data

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import java.util.*

@Dao
interface NoteDao {

    @Query("SELECT * FROM note")
    fun getAll(): LiveData<List<Note>>

    @Query("SELECT * FROM note WHERE uid == :noteId")
    fun findById(noteId: UUID): LiveData<Note>

    @Insert(onConflict = REPLACE)
    suspend fun insert(note:Note)

    @Update(onConflict = REPLACE)
    suspend fun update(note:Note)

    @Query("DELETE FROM note where uid==:noteId")
    suspend fun delete(noteId:UUID)

    @Query("DELETE FROM note")
    suspend fun deleteAll()
}