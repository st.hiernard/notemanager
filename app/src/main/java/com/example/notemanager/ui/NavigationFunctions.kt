package com.example.notemanager.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.example.notemanager.R

//Used with bundle passing data method
fun navigateToNoteDetailFragment(f: Fragment, bundle: Bundle? = null){
    findNavController(f).navigate(R.id.action_noteListFragment_to_noteDetailFragment, bundle)
}

//Used with bundle passing data method 
fun navigateToNoteListFragment(f:Fragment, bundle: Bundle? = null){
    findNavController(f).navigate(R.id.action_noteDetailFragment_to_noteListFragment, bundle)
}