package com.example.notemanager.ui

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.notemanager.R
import com.example.notemanager.data.Note
import com.example.notemanager.utils.NOTE_ID_BUNDLE_ARGUMENT
import com.example.notemanager.viewmodels.NoteListViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton


class NoteListFragment : Fragment() {

    companion object {
        fun newInstance() = NoteListFragment()
    }

    private lateinit var viewModel: NoteListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.note_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NoteListViewModel::class.java)

        //Affichage de la liste des notes
        viewModel.getAllNotes().observe(viewLifecycleOwner, Observer<List<Note>>{ notes : List<Note> ->
            val noteArray = ArrayAdapter(this.requireContext(), android.R.layout.simple_list_item_1, notes)
            val noteListView = view?.findViewById<ListView>(R.id.noteList)
            noteListView?.adapter = noteArray
            //Si on cliue sur une note, on change de fragment
            noteListView?.setOnItemClickListener { parent, view, position, id ->
                val noteClicked = noteArray.getItem(position)

                //METHOD 1 to pass data to fragment destination : using bundle
                /*var bundle = bundleOf(NOTE_ID_BUNDLE_ARGUMENT to  noteClicked?.uid.toString())
                navigateToNoteDetailFragment(this, bundle)*/

                //METHOD 2 to pass data to fragment destination : using safe args from navigation controller
                view.findNavController().navigate(NoteListFragmentDirections.actionNoteListFragmentToNoteDetailFragment(noteClicked?.uid.toString()))

            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Affichage d'une vue modale pour créer une note
        view.findViewById<FloatingActionButton>(R.id.addnoteButton).setOnClickListener {
            val dialog = Dialog(this.requireContext())
            dialog.setContentView(R.layout.add_note_dialog)
            dialog.setCancelable(false) //On ne peut pas quitter la fenêtre modale en cliquant en dehors

            dialog.findViewById<Button>(R.id.validateAddNoteButton).setOnClickListener{
                
                val noteTitleStr = dialog.findViewById<EditText>(R.id.noteTitleEditText).text.toString() //Le titre de la nouvelle note

                if(noteTitleStr.isEmpty()){
                    Toast.makeText(requireContext(), resources.getText(R.string.emptyTitle), Toast.LENGTH_SHORT).show()
                } else {
                    if (dialog.isShowing) dialog.cancel()

                    val newNoteId = viewModel.addNote(noteTitleStr).toString()

                    Log.d("uuid",newNoteId)

                    //METHOD 1 to pass data to fragment destination : using bundle
                    /*var bundle = bundleOf(NOTE_ID_BUNDLE_ARGUMENT to  newNoteId)
                    navigateToNoteDetailFragment(this, bundle)*/

                    //METHOD 2 to pass data to fragment destination : using safe args from navigation controller
                    view.findNavController().navigate(NoteListFragmentDirections.actionNoteListFragmentToNoteDetailFragment(newNoteId))
                }
            }

            dialog.findViewById<Button>(R.id.cancelAddNoteButton).setOnClickListener{
                dialog.cancel()
            }

            dialog.show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.deleteAllNotesSetting -> {
                viewModel.deleteAllNotes()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}