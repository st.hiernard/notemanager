package com.example.notemanager.ui

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.notemanager.R
import com.example.notemanager.data.Note
import com.example.notemanager.utils.NOTE_ID_BUNDLE_ARGUMENT
import com.example.notemanager.viewmodels.NoteDetailViewModel
import java.text.SimpleDateFormat
import java.util.*

class NoteDetailFragment : Fragment() {

    companion object {
        fun newInstance() = NoteDetailFragment()
    }

    private lateinit var viewModel: NoteDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.note_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(NoteDetailViewModel::class.java)

        //METHOD 1 to retrieve data from fragment source : using bundle
        //val noteId = UUID.fromString(arguments?.getString(NOTE_ID_BUNDLE_ARGUMENT)?:"")

        //METHOD 2 to retrieve data from fragment source : using safe args from navigation controller
        val args:NoteDetailFragmentArgs by navArgs()
        val noteId =  UUID.fromString(args.noteId)

        Log.d("uuid",noteId.toString())

        //Affiche les infos de la note
        viewModel.getNote(noteId).observe(viewLifecycleOwner, Observer<Note>{note : Note ->
            if(!viewModel.isTitleSpecified) {
                view?.findViewById<TextView>(R.id.noteTitle)?.text = note.title
                viewModel.isTitleSpecified = true
            }
            if(!viewModel.isTextSpecified) {
                view?.findViewById<EditText>(R.id.noteText)?.setText(note.text)
                viewModel.isTextSpecified = true
            }
            if(!viewModel.isCreationDateSpecified) {
                view?.findViewById<TextView>(R.id.noteCreationDate)?.setText("${getString(R.string.createdThe)} ${SimpleDateFormat("dd/MM/yyyy").format(note.creationDate)}")
                viewModel.isCreationDateSpecified = true
            }
        })

        //Synchronise le texte de la note et la database
        view?.findViewById<EditText>(R.id.noteText)?.addTextChangedListener(onTextChanged = {s: CharSequence?, start: Int, before: Int, count: Int ->
            Log.d("note_text", view?.findViewById<EditText>(R.id.noteText)?.text.toString())
            viewModel.updateNote(view?.findViewById<EditText>(R.id.noteText)?.text.toString())
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.note_detail_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.deleteNoteSetting -> {


                //METHOD 1 to retrieve data from fragment source : using bundle
                //navigateToNoteListFragment(this)
                //val noteId = UUID.fromString(arguments?.getString(NOTE_ID_BUNDLE_ARGUMENT)?:"")

                //METHOD 2 to retrieve data from fragment source : using safe args from navigation controller
                findNavController().navigate(NoteDetailFragmentDirections.actionNoteDetailFragmentToNoteListFragment())
                val args:NoteDetailFragmentArgs by navArgs()
                val noteId =  UUID.fromString(args.noteId)

                viewModel.deleteNote(noteId)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}