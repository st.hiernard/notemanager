package com.example.notemanager.utils

import androidx.room.TypeConverter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class Converter {
    @TypeConverter
    fun stringToDate(dateStr:String): Date = DateFormat.getDateInstance().parse(dateStr)

    @TypeConverter
    fun dateToString(date: Date): String = DateFormat.getDateInstance().format(date)

    @TypeConverter
    fun UUIDToString(uid:UUID) = uid.toString()

    @TypeConverter
    fun StringToUUID(str:String) = UUID.fromString(str)

}